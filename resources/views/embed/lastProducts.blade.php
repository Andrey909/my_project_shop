<div class="container">
    <div class="row">

        <div class="col-md-12">
            <h2>Last added products:</h2>
        </div>

        @foreach($freshProducts as $product)

            <div class="col-md-4">
                <h3>{{ $product['title'] }}</h3>
                <p> {{ $product['price'] }} </p>
                <p> {{ $product['description'] }} </p>
                <p><a class="btn btn-primary" href="/products/{{ $product['slug'] }}" role="button">View details »</a></p>
                <p><a href="/cart/{{$product['slug']}}" class="btn btn-success">Buy</a></p>
            </div>

        @endforeach
    </div>
</div>
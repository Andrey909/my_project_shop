<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="col-2 d-flex justify-content-end align-items-center">
                <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
            </li>
            @if(Auth::check())
            <li class="col-3 d-flex justify-content-end align-items-center">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Shop</a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="/products/create">Create</a>
                    <a class="dropdown-item" href="/products">Show</a>
                </div>
            </li>
            @else
                <li class="col-2 d-flex justify-content-end align-items-center">
                    <a class="nav-link" href="/products">Shop</a>
                </li>
            @endif

                <li class="col-2 d-flex justify-content-end align-items-center">
                    <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Post</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">

                <a class="dropdown-item" href="/posts/create">Add Post</a>


                <a class="dropdown-item" href="/">Post</a>


            <li class="col-3 d-flex justify-content-end align-items-center">
                <a class="nav-link" href="/summary">Summary</a>
            </li>
            <li class="col-7 d-flex justify-content-end align-items-center">
                <a class="nav-link" href="/cart">Cart</a>
            </li>
            <li class="col-1 d-flex justify-content-end align-items-center">
                <a class="nav-link" href="/order">Order</a>
            </li>

        @if(Auth::check())
                <li class="nav-item">
                    <a class="nav-link" href="/admin">admin</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">{{Auth::user()->name}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/logout">Logout</a>
                </li>

            @else


                <li class="col-2 d-flex justify-content-end align-items-center">
                    <a class="nav-link" href="/login">Login</a>
                </li>
                <li class="col-2 d-flex justify-content-end align-items-center">

                    <a class="btn btn-sm btn-outline-secondary" href="/register">Sign up</a>
                </li>

            @endif



        </ul>
    </div>
</nav>


<!doctyp html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Carousel Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="carousel.css" rel="stylesheet">
</head>
<body>


@include('embed.header')
<main role="main">






    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

        <!-- Three columns of text below the carousel -->


        <br>
        <br>
        <br>

        <h1>Andrei Martynenko</h1>
        <h3>Junior PHP developer</h3>
        <div class="row featurette">
            <div class="col-md-7">
                <p class="lead">Personal information</p>
                <p class="lead">Address: Odessa</p>
                <p class="lead">Phone number: 0970796812</p>
                <p class="lead">E-mail: martin.dm909@gmail.com</p>
                <p class="lead">Skype: martin72586</p>
                <p class="lead">Linkedin: www.linkedin.com/in/andrey-martynenko-0970796812</p>
                <p class="lead">Bitbucket: https://bitbucket.org/Andrey909/</p>
            </div>

            <div> <img src="/public/uploads/f1.jpg"  class="img-thumbnail"></div>

        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-5">
                <h2 class="featurette-heading">Skills</h2>

            </div>
            <div class="col-md-7">

                <p class="lead">1. HTML5, CSS3, Bootstrap</p>
                <p class="lead">2. Understanding the principles ООP, MVC</p>
                <p class="lead">3. Knowledge of PHP language</p>
                <p class="lead">4. Framework (Laravel 5)</p>
                <p class="lead">5. MySQL</p>
                <p class="lead">6. Basics Git, Composer</p>
                <p class="lead">7. Developer Tools: Sublime Text 3, PhpStorm</p>
            </div>


        </div>

        <hr class="featurette-divider">

        <div class="row featurette">

            <div class="col-md-5">
                <h2 class="featurette-heading">Education</h2>


            </div>
            <div class="col-md-7">
            <p class="lead">IT School Hillel
                Course of PHP
                2017-2018</p>
            <p class="lead">Odesa I. I. Mechnikov National University
                Faculty of Economics and Law
                2010-2015</p>
            </div>
        </div>

        <hr class="featurette-divider">

        <!-- /END THE FEATURETTES -->

    </div><!-- /.container -->


    <!-- FOOTER -->
    <footer class="container">
        <p class="float-right"><a href="#">Back to top</a></p>
        <p>&copy; 2018 Summary Martynenko </p>
    </footer>
</main>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>

<!-- Just to make our placeholder images work. Don't actually copy the next line! -->

</body>
</html>
